cd C:\Python
echo "Installing Pandas Python Library"
pip install pandas
echo "Install Beautiful Soup for Web Scraping"
pip install bs4
echo "Install Numpy Python Library"
pip install numpy
echo "Installing PuLP Solving Engine"
pip install pulp
echo "Installation is complete"
