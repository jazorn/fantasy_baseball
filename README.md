Collection of Codes for Fantasy Sports, specifically Fantasy Baseball
Written by Jacob Zorn
Pennylvania State University
Penn State Sabermetrics Club

This repository contains a optimization software for the diehard fantasy baseball and fantasy sports fan. All of the codes presented here have been tested extensively on Windows 7 and Windows 10. It is expected that since the software was written in cross-platform languages, the interested user should be able to easily port them over to their system of choice.

The current collection of codes includes Optimization routines for deriving the best daily fantasy lineup. These codes, an installation batch file, and an example input csv file are provided in the directory to help you get started. All of the codes, input files, installation instructions, and other necessary information for the daily fantasy sports predictions are store in the DFS Directory. 

**Disclaimer**
Due to potential liability and fair-use guidelines I will not share any web scraping or data/information sources for the development of projections and other necessary information.


Languages:
*  Python
*  VBA

Further Developments to come:
*  Graphic Interface
*  Offline Fantasy Draft Software in Python
